import cv2
import random
import colorsys
import numpy as np

def load_freeze_layer(model='yolov4'):
    freeze_layouts = ['conv2d_93', 'conv2d_101', 'conv2d_109']
    return freeze_layouts